package icm.ua.pt.projetoicm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MenuPrincipal extends Fragment {

    private ListView noticias;
    private FirebaseDatabase database;
    NoticiaAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_menu_principal, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Nos Alive");
        noticias = (ListView) getActivity().findViewById(R.id.listMenuPrincipal);

        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("FESTIVAIS");

        ref.addValueEventListener(new ValueEventListener() {
            ArrayList<Noticia> nt = new ArrayList<>();

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> festival = dataSnapshot.getChildren();

                for (DataSnapshot f : festival) {
                    for (DataSnapshot n : f.getChildren()) {
                        Noticia noti = n.getValue(Noticia.class);
                        nt.add(noti);
                    }
                }

                adapter = new NoticiaAdapter(getActivity(),nt);
                noticias.setAdapter(adapter);
                final Context context = getActivity();
                noticias.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Noticia selectedNoticia = nt.get(position);

                        Intent detailIntent = new Intent(context, NoticiaDetailActivity.class);

                        detailIntent.putExtra("titulo", selectedNoticia.titulo);
                        detailIntent.putExtra("corpo", selectedNoticia.corpo);
                        detailIntent.putExtra("linkimagem", selectedNoticia.linkimagem);
                        detailIntent.putExtra("linkurl", selectedNoticia.linkurl);

                        startActivity(detailIntent);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }
}
