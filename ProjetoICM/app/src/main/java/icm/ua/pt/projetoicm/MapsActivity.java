package icm.ua.pt.projetoicm;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        LatLng alive = new LatLng(38.697148, -9.231731);
        LatLng rock = new LatLng(38.749868, -9.125541);
        LatLng sbsr = new LatLng(38.768468, -9.094046);
        LatLng meo = new LatLng(37.524329, -8.784237);
        LatLng mexeFest = new LatLng(38.716748,-9.1403145);
        LatLng portugal = new LatLng(39.6521741,-7.7354591);
        mMap.addMarker(new MarkerOptions().position(alive).title("NOS ALIVE"));
        mMap.addMarker(new MarkerOptions().position(rock).title("ROCK IN RIO"));
        mMap.addMarker(new MarkerOptions().position(sbsr).title("SUPER BOCK SUPER ROCK"));
        mMap.addMarker(new MarkerOptions().position(meo).title("MEO SUDOESTE"));
        mMap.addMarker(new MarkerOptions().position(mexeFest).title("Vodafone Mexe Fest"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(portugal, 7));

    }

}
