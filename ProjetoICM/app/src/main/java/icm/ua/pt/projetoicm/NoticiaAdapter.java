package icm.ua.pt.projetoicm;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by josesa on 26/11/2016.
 */

public class NoticiaAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Noticia> mDataSource;

    public NoticiaAdapter(Context context, ArrayList<Noticia> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.list_item_recipe, parent, false);

        TextView titleTextView =
                (TextView) rowView.findViewById(icm.ua.pt.projetoicm.R.id.recipe_list_title);

        TextView subtitleTextView =
                (TextView) rowView.findViewById(icm.ua.pt.projetoicm.R.id.recipe_list_subtitle);

        TextView detailTextView =
                (TextView) rowView.findViewById(icm.ua.pt.projetoicm.R.id.recipe_list_detail);

        ImageView thumbnailImageView =
                (ImageView) rowView.findViewById(icm.ua.pt.projetoicm.R.id.recipe_list_thumbnail);

        Noticia n = (Noticia) getItem(position);

        titleTextView.setText(n.titulo);
        subtitleTextView.setText(n.corpo);
        detailTextView.setText("Ver");

        if(n.id==1) {
            Picasso.with(mContext).load("https://firebasestorage.googleapis.com/v0/b/api-project-1059817165761.appspot.com/o/nos.png?alt=media&token=4ded0998-0f35-4ab9-84d7-c5e0d6ce2373").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);
        }
        else if (n.id==3)
            Picasso.with(mContext).load("https://firebasestorage.googleapis.com/v0/b/api-project-1059817165761.appspot.com/o/sbsr.png?alt=media&token=e7be860a-b3ed-465b-ae97-c8f0077bbe38").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);
        else if (n.id == 0)
            Picasso.with(mContext).load("https://firebasestorage.googleapis.com/v0/b/api-project-1059817165761.appspot.com/o/meo.png?alt=media&token=7f968cbe-3d51-4efd-a74c-f711d72673ba").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);
        else if (n.id==2)
            Picasso.with(mContext).load("https://firebasestorage.googleapis.com/v0/b/api-project-1059817165761.appspot.com/o/rock.jpg?alt=media&token=6d01045e-d351-4ced-abbb-bd803ed7fdb3").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);
        else if (n.id==4)
            Picasso.with(mContext).load("https://firebasestorage.googleapis.com/v0/b/api-project-1059817165761.appspot.com/o/vodafone.jpg?alt=media&token=e2bf3f28-ada0-4c43-8537-41953bf626e2").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView);

        Typeface titleTypeFace = Typeface.createFromAsset(mContext.getAssets(), "fonts/JosefinSans-Bold.ttf");
        titleTextView.setTypeface(titleTypeFace);

        Typeface subtitleTypeFace =
                Typeface.createFromAsset(mContext.getAssets(), "fonts/JosefinSans-SemiBoldItalic.ttf");
        subtitleTextView.setTypeface(subtitleTypeFace);

        Typeface detailTypeFace = Typeface.createFromAsset(mContext.getAssets(), "fonts/Quicksand-Bold.otf");
        detailTextView.setTypeface(detailTypeFace);

        return rowView;
    }
}
