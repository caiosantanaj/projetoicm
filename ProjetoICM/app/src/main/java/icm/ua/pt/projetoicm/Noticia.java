package icm.ua.pt.projetoicm;

/**
 * Created by josesa on 26/11/2016.
 */

public class Noticia {

    int id;
    String titulo;
    String corpo;
    String linkimagem;
    String linkurl;

    public Noticia(){

    }

    public Noticia(String corpo, int id, String linkimagem, String linkurl, String titulo){
        this.id=id;
        this.titulo = titulo;
        this.corpo = corpo;
        this.linkimagem = linkimagem;
        this.linkurl = linkurl;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCorpo() {
        return corpo;
    }

    public void setCorpo(String corpo) {
        this.corpo = corpo;
    }

    public String getLinkimage() {
        return linkimagem;
    }

    public void setLinkimage(String linkimagem) {
        this.linkimagem = linkimagem;
    }

    public String getLinkurl() {
        return linkurl;
    }

    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
