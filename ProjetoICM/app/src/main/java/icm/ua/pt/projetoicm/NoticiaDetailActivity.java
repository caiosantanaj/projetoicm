package icm.ua.pt.projetoicm;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Parcel;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NoticiaDetailActivity extends AppCompatActivity {

    private Bitmap image;
    int counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_noticia_detail);
        counter = 0;

        String tituloString = this.getIntent().getExtras().getString("titulo");
        String corpoString = this.getIntent().getExtras().getString("corpo");
        String linkimagemString = this.getIntent().getExtras().getString("linkimagem");
        String linkurlString = this.getIntent().getExtras().getString("linkurl");
        setTitle(tituloString);

        TextView titulo = (TextView) findViewById(R.id.titulo);
        tituloString.toUpperCase();
        titulo.append(tituloString);

        TextView corpo = (TextView) findViewById(R.id.corpo);
        corpo.append(corpoString);

        ImageView linkimagem = (ImageView) findViewById(R.id.linkimagem);
        Picasso.with(getApplicationContext()).load(linkimagemString).placeholder(R.mipmap.ic_launcher).into(linkimagem);

        Typeface titleTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/JosefinSans-Bold.ttf");
        titulo.setTypeface(titleTypeFace);

        Typeface subtitleTypeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/JosefinSans-SemiBoldItalic.ttf");
        corpo.setTypeface(subtitleTypeFace);

        final ShareButton shareButton = (ShareButton) findViewById(R.id.share_btn);
        Log.i("Facebook", "facebook");
        ShareLinkContent content = new ShareLinkContent.Builder().setContentUrl(Uri.parse(linkurlString)).build();
        shareButton.setShareContent(content);
    }
}
