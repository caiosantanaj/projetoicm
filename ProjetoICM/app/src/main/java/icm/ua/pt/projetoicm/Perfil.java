package icm.ua.pt.projetoicm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

public class Perfil extends Fragment {

    EditText nome;
    EditText telemovel;
    EditText mail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_perfil, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles

        nome = (EditText) getActivity().findViewById(R.id.nome);
        telemovel = (EditText) getActivity().findViewById(R.id.telemovel);
        mail = (EditText) getActivity().findViewById(R.id.mail);

        nome.setText(((MainActivity) getActivity()).getNome());
        telemovel.setText(((MainActivity) getActivity()).getTelemovel());
        mail.setText(((MainActivity) getActivity()).getMail());

        Button ok = (Button) getActivity().findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).setNome(nome.getText().toString());
                ((MainActivity) getActivity()).setTelemovel(telemovel.getText().toString());
                ((MainActivity) getActivity()).setMail(mail.getText().toString());
            }
        });

        String url = ((MainActivity)getActivity()).getmPhotoUrl();
        SelectableRoundedImageView image = (SelectableRoundedImageView) getActivity().findViewById(R.id.image);
        Picasso.with(getActivity()).load(url).placeholder(R.mipmap.ic_launcher).into(image);
    }
}
