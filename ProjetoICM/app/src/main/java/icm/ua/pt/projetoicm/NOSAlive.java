package icm.ua.pt.projetoicm;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;


public class NOSAlive extends Fragment {

    private ListView noticias;
    private FirebaseDatabase database;
    private String url;
    private Button cartaz;
    private Button direcaoButton;
    private Button bilhetesButton;
    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nosalive, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Nos Alive");

        noticias = (ListView) getActivity().findViewById(R.id.listanosalive);
        cartaz = (Button) getActivity().findViewById(R.id.cartaz);
        cartaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(EXTRA_MESSAGE, url);
                startActivity(intent);
            }
        });

        direcaoButton = (Button) getActivity().findViewById(R.id.direcaoAlive);
        direcaoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = String.format(Locale.ENGLISH, "geo:38.697148,-9.231731");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                getActivity().startActivity(intent);
            }
        });

        bilhetesButton = (Button) getActivity().findViewById(R.id.bilhetes);
        bilhetesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "http://nosalive.com/bilhetes/");
                startActivity(intent);
            }
        });

        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("FESTIVAIS");
        //ok
        ref.addValueEventListener(new ValueEventListener() {
            ArrayList<Noticia> nt = new ArrayList<>();
            NoticiaAdapter adapter;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> festival = dataSnapshot.getChildren();
                for (DataSnapshot f : festival) {
                    for (DataSnapshot n : f.getChildren()) {
                        Noticia noti = n.getValue(Noticia.class);
                        if(noti.getId()==1) {
                            nt.add(noti);
                        }
                    }
                }
                adapter = new NoticiaAdapter(getActivity(),nt);
                noticias.setAdapter(adapter);
                final Context context = getActivity();
                noticias.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Noticia selectedNoticia = nt.get(position);

                        Intent detailIntent = new Intent(context, NoticiaDetailActivity.class);

                        detailIntent.putExtra("titulo", selectedNoticia.titulo);
                        detailIntent.putExtra("corpo", selectedNoticia.corpo);
                        detailIntent.putExtra("linkimagem", selectedNoticia.linkimagem);
                        detailIntent.putExtra("linkurl", selectedNoticia.linkurl);

                        startActivity(detailIntent);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }
}
